﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    [SerializeField] private Text WiningScore;
    [SerializeField] private Text GameOverText;
    [SerializeField] private GameObject GameOverForm;

    public void UpdateWinsText()
    {
        int losing = PlayerPrefs.GetInt(Defines.WIN_COUNT_KEY + 0, 0);
        int win = PlayerPrefs.GetInt(Defines.WIN_COUNT_KEY + 1, 0);
        int equals = PlayerPrefs.GetInt(Defines.EQUALS_COUNT_KEY, 0);
        WiningScore.text = "Win: " + win + " Draw: " + equals + " Losing: " + losing;
    }

    public void MenuOpen()
    {
        SceneManager.LoadScene(Defines.MenuScene);
    }

    public void SetGameOverForm(bool active)
    {
        GameOverForm.gameObject.SetActive(active);
    }

    public void SetGameOverText(String msg)
    {
        GameOverText.text = msg;
    }
}
