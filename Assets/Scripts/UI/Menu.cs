﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public enum GameMode
{
    Easy,
    Medium,
    Impossible
}

public class Menu : MonoBehaviour
{
    public Text GameModeButtonText;

    private void Awake()
    {
        UpdateGameMode(true);
    }

    public void UpdateGameMode(bool initial)
    {
        int mode = PlayerPrefs.GetInt(Defines.GAME_MODE_KEY, -1);
        if(!initial || mode == -1)
            mode = (++mode) % Enum.GetNames(typeof(GameMode)).Length;
        PlayerPrefs.SetInt(Defines.GAME_MODE_KEY, mode);
        GameModeButtonText.text = ((GameMode)mode).ToString();
    }

    public void StartNewGame()
    {
        SceneManager.LoadScene(Defines.GameScene);
    }
}
