﻿using System;
using UnityEngine;
using System.Collections;

public class GameField : MonoBehaviour 
{
    public SpriteRenderer [] Cells = new SpriteRenderer[9];

    public void UpdateView(MatrixPos pos, Element el)
    {
        String name;
        if (el == Element.None)
            name = ResourceManager.NoSprite;
        else
            name = "el" + el;
        Cells[pos.x + pos.y*3].sprite = ResourceManager.Instance.GetSprite(name);
    }

}
