﻿using System;
using System.Collections.Generic;
using UnityEngine;


public class ResourceManager : MonoBehaviour
{
    
    private readonly Dictionary<string, Sprite> sprites = new Dictionary<string, Sprite>();
    public static ResourceManager Instance { get; private set; }


    public const string NoSprite = "none";

    void Awake()
    {
        Init();
    }

    public void Init()
    {
        if (Instance == null)
        {
            Instance = this;
            UpdateAtlas();
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Debug.LogWarning("Possible multiple init ResourceManager");
        }
    }

    void UpdateAtlas()
    {
        sprites.Clear();
        // load all sprites to dictionary
        foreach (var sprite in Resources.LoadAll<Sprite>(""))
            sprites.Add(sprite.name, sprite);
    }

    public Sprite GetSprite(string spriteName)
    {
        if (spriteName == NoSprite)
            return null;

        Sprite result;
        if (sprites.TryGetValue(spriteName, out result))
            return result;

        Debug.LogError("no sprite found by name " + spriteName);
        return null;
    }

#if UNITY_EDITOR
    [ContextMenu("Update Atlas")]
    public void UpdateAtlasMenu()
    {
        UpdateAtlas();
        foreach (var str in sprites.Keys)
        {
            Debug.Log(str); 
        }
    }
#endif

}