﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class CameraSize : MonoBehaviour
{
    //[SerializeField] private float currRatio = 0;
    void Awake()
    {
        SetCameraSettings();
    }

    public static void SetCameraSettings()
    {
        float cameraSize = (Defines.GameSize.x * Screen.height) / (Screen.width * Defines.PixelsToUnit);
        Camera.main.orthographicSize = cameraSize;
    }

#if UNITY_EDITOR
    public static Vector2 GetMainGameViewSize()
    {
        System.Type T = System.Type.GetType("UnityEditor.GameView,UnityEditor");
        System.Reflection.MethodInfo GetSizeOfMainGameView = T.GetMethod("GetSizeOfMainGameView", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);
        System.Object Res = GetSizeOfMainGameView.Invoke(null, null);
        return (Vector2)Res;
    }

    [ContextMenu("SetEditorCameraSettings")]
    public void SetEditorCameraSettings()
    {
        Vector2 screenSize = GetMainGameViewSize();
        float cameraSize = (Defines.GameSize.x * screenSize.y) / (screenSize.x * Defines.PixelsToUnit);
        Camera.main.orthographicSize = cameraSize;
    }

#endif
}
