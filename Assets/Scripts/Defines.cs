﻿using UnityEngine;
using System.Collections;

public enum PlayerState
{
    Player = 1,
    AI = 0
}

public static class Defines
{
    public const string MenuScene = "Menu";
    public const string GameScene = "Game";

    public const string GAME_MODE_KEY = "GAME_MODE";
    public const string LAST_WINNER_KEY = "LAST_WINNER";
    public const string WIN_COUNT_KEY = "WIN_COUNT_";
    public const string EQUALS_COUNT_KEY = "EQUALS_COUNT";

    public static Vector2 GameSize = new Vector2(340f, 327f);
    public static float PixelsToUnit = 100f;

}
