﻿using UnityEngine;
using System.Collections;

public class AIo : AIStrategic
{

    public override Element Sign
    {
        get { return Element.Tac; }
        set { }
    }
    private int[] s1 = { -1, 1, 0, 4, 7, 3};
    private int[] s2 = { -1, 1, 3 };

    private int[] s3 = { 1, -1, 5, 0 };
    private int[] s4 = { 0, -1};

    public override void Init()
    {
        base.Init();
        Strategics.Clear();
        Strategics.Add(s1);
        Strategics.Add(s2);
        Strategics.Add(s3);
        Strategics.Add(s4);
        /*Strategics.Add(s5);
        Strategics.Add(s6);
        Strategics.Add(s7);
        Strategics.Add(s8);
        Strategics.Add(s9);
        Strategics.Add(s10);*/
        //DebugStrategics();
    }
}