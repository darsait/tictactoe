﻿using UnityEngine;
using System.Collections;

public class AIMedium : AI
{
    private Element sign;
    public override Element Sign
    {
        get { return sign; }
        set { sign = value; }
    }

    public override void OnMove()
    {
    }

    public override void Init()
    {
    }

    public override MatrixPos GetMove()
    {
        int i;
        MatrixPos pos;

        pos = Field.GetWinMove(Sign);
        if (pos != MatrixPos.Empty)
            return pos;

        pos = Field.GetWinMove(Field.GetOpponent(Sign));
        if (pos != MatrixPos.Empty)
            return pos;

        if (Field.IsCellFree(MatrixPos.One))
            return MatrixPos.One;

        pos = GetConerMove();
        if (pos != MatrixPos.Empty)
            return pos;

        pos = GetSideMove();
        return pos;

    }	
}