﻿using System;
using System.Linq;
using UnityEngine;
using System.Collections;

public class Controller : MonoBehaviour
{
	
    [SerializeField] public GameField GameField;
    [SerializeField] public UI UI;
	[NonSerialized] public int Player = 1;
    [NonSerialized] public Element[] PlayerSign = new Element[2];
    [NonSerialized] public GameStates[] PlayerStates = new GameStates[2];
    public Field Field;
    public AI OpponentAI;


    void Awake()
    {
        if (Phase.Controller == null)
        {
            Phase.Controller = this;
        }
    }

	void Start () 
	{
        Field = new Field();
        Field.UpdateView = GameField.UpdateView;
        Field.Init();
	    AI.Field = Field;

		//Instance = this;
        PlayerStates[0] = GameStates.AIPhase;
        PlayerStates[1] = GameStates.PlayerPhase;
        UI.UpdateWinsText();
        UI.SetGameOverForm(false);

        InitPhases.Init();
        Phase.Start(GameStates.StartNewGame.ToString());
    }
	
	
	public bool IsNoSpace()
	{
	    return !Field.GetEmptyPositions().Any();
	}
	
	public void StartNewGame()
	{
        Phase.Start(GameStates.StartNewGame.ToString());
    }
	
    void OnMouseUp()
    {
        Vector2 currMousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        var pos = Field.CalcGamePosition(currMousePos);
        Phase.CurrentPhase.OnInput(pos);
    }

    public void OnEndOfGame(Element state)
    {
        String key;
        String msg;
        if (state != Element.None)
        {
            key = Defines.WIN_COUNT_KEY + Player;
            msg = state == PlayerSign[1] ? "You Win" : "You Lose";
        }
        else
        {
            key = Defines.EQUALS_COUNT_KEY;
            msg = "Draw";
        }
        int winCount = PlayerPrefs.GetInt(key, 0);
        PlayerPrefs.SetInt(key, ++winCount);
        UI.UpdateWinsText();
        UI.SetGameOverText(msg);
        int lastWinner;
        if (state != Element.None || PlayerSign[Player] == Element.Tac)
            lastWinner = Player;
        else
            lastWinner = (Player + 1) % 2;
        PlayerPrefs.SetInt(Defines.LAST_WINNER_KEY, lastWinner);
    }
}
