﻿using UnityEngine;

public class StartNewGame : Phase
{
    public override void OnPhaseStart()
    {
        Controller.Field.Clear();
        Controller.Player = PlayerPrefs.GetInt(Defines.LAST_WINNER_KEY, (int)PlayerState.Player);
        Controller.PlayerSign[Controller.Player] = Element.Tic;
        Controller.PlayerSign[(Controller.Player + 1)%2] = Element.Tac;
        GameMode mode = (GameMode)PlayerPrefs.GetInt(Defines.GAME_MODE_KEY, (int)GameMode.Easy);
        switch (mode)
        {
            case GameMode.Easy:
                Controller.OpponentAI = new AIEasy();
                break;
            case GameMode.Medium:
                Controller.OpponentAI = new AIEasy();
                break;
            case GameMode.Impossible:
                switch (Controller.PlayerSign[(int) PlayerState.AI])
                {
                    case Element.Tic:
                        Controller.OpponentAI = new AIx();
                        break;
                    case Element.Tac:
                        Controller.OpponentAI = new AIo();
                        break;
                }
                break;

        }
        Controller.OpponentAI.Sign = Controller.PlayerSign[(int) PlayerState.AI];
        Controller.OpponentAI.Init();
        Start(Controller.PlayerStates[Controller.Player].ToString());
    }
}
