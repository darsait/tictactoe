﻿using UnityEngine;

public class EndOfGame : Phase
{
    public override void OnPhaseStart()
    {
        Controller.UI.SetGameOverForm(true);
    }

    public override void OnPhaseEnd()
    {
        Controller.UI.SetGameOverForm(false);
    }
}
