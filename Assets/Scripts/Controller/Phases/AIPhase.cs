﻿using UnityEngine;

public class AIPhase : Phase
{
    public override void OnPhaseStart()
    {
        Controller.Field.SetElement(Controller.OpponentAI.GetMove(), Controller.PlayerSign[Controller.Player]);
        Controller.OpponentAI.OnMove();
        Start(GameStates.EndOfTurn.ToString());
    }

}
