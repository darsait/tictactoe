﻿using System;
using UnityEngine;

public class EndOfTurn : Phase
{
    public override void OnPhaseStart()
    {
		var win = Controller.Field.CheckWin();

        if (win != Element.None || Controller.IsNoSpace())
        {
            Controller.OnEndOfGame(win);
            Start(GameStates.EndOfGame.ToString());
        }
		else 
		{
            Controller.Player = ++Controller.Player%2;
            Start(Controller.PlayerStates[Controller.Player].ToString());
        }
    }
}
