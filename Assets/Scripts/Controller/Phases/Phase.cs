﻿using System;
using System.Collections.Generic;


public abstract class Phase
{
    public static Controller Controller;
    public static Phase CurrentPhase { get; protected set; }
    private static Dictionary<String, Phase> automat = new Dictionary<string, Phase>();
    protected static bool inited = false;

    public static void Register(String name, Phase instance)
    {
        instance.Init();
        automat.Add(name, instance);
    }

    //public delegate void OnHintEvent(string Hint);
    //public static event OnHintEvent OnHint = null;

    public static void Start(string name)
    {
        if (CurrentPhase != null)
        {
            End();
        }
        Phase phase;
        if (automat.TryGetValue(name, out phase))
        {
            CurrentPhase = phase;
            CurrentPhase.OnPhaseStart();
        }
    }

    public static void End()
    {
        CurrentPhase.OnPhaseEnd();
        CurrentPhase = null;
    }

    public virtual void OnPhaseStart()
    {
        CurrentPhase = this;
        //UpdateHint();
    }

    public virtual void OnPhaseEnd()
    {

    }
    public virtual void Pause()
    {

    }

    public virtual void Restart()
    {
        //UpdateHint();
    }

    public virtual void Init() { }
    public virtual void OnInput(MatrixPos pos) { }
}

