﻿using UnityEngine;

public class PlayerPhase : Phase
{
    public override void OnPhaseStart()
    {
    }

    public override void OnPhaseEnd()
    {
    }

    public override void OnInput(MatrixPos pos)
    {
        Controller.Field.SetElement(pos, Controller.PlayerSign[Controller.Player]);
        Start(GameStates.EndOfTurn.ToString());
    }
}
