﻿using UnityEngine;

public enum GameStates
{
    StartNewGame,
    PlayerPhase,
    AIPhase,
    EndOfTurn,
    EndOfGame
}

public class InitPhases : Phase
{
    public static void Init()
    {
        if (!inited)
        {
            Register(GameStates.StartNewGame.ToString(), new StartNewGame());
            Register(GameStates.PlayerPhase.ToString(), new PlayerPhase());
            Register(GameStates.AIPhase.ToString(), new AIPhase());
            Register(GameStates.EndOfTurn.ToString(), new EndOfTurn());
            Register(GameStates.EndOfGame.ToString(), new EndOfGame());
            inited = true;
        }
    }
}
