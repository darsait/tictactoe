﻿using UnityEngine;
using System.Collections;

public class AIx : AIStrategic
{
    private int[] s1 = {-1, 0, 1, 5, 2 };
    private int[] s2 = {-1, 0, 1, 5, 3 };
    private int[] s3 = {-1, 0, 2, 6, 1 };
    private int[] s4 = {-1, 0, 2, 6, 3 };
    private int[] s5 = {-1, 0, 3, 7, 1 };

    private int[] s6 = {-1, 1, 3, 7, 8, 4, 6 };

    private int[] s7 = { 1, 2, -1, 5, 7};
    private int[] s8 = { 1, 3, 7, 0, 5};
    private int[] s9 = { 1, 5, 3, 2, 7 };
    private int[] s10 = { 1, -1, 5, 3, 7 };

    public override Element Sign
    {
        get { return Element.Tic; }
        set {  }
    }

    public override void Init()
    {
        base.Init();
        Strategics.Clear();
        Strategics.Add(s1);
        Strategics.Add(s2);
        Strategics.Add(s3);
        Strategics.Add(s4);
        Strategics.Add(s5);
        Strategics.Add(s6);
        Strategics.Add(s7);
        Strategics.Add(s8);
        Strategics.Add(s9);
        Strategics.Add(s10);
        //DebugStrategics();
    }
}
