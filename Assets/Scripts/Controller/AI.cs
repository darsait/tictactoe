﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;


public abstract class AI 
{
    public static Field Field = null;

    public abstract Element Sign { get; set; }
    public abstract MatrixPos GetMove();
    public abstract void OnMove();
    public abstract void Init();

    protected virtual MatrixPos GetCell(int i)
    {
        if (i == -1)
            return MatrixPos.One;
        return MatrixPos.Dir8[i] + MatrixPos.One;
    }

    protected virtual MatrixPos GetConerMove()
    {
        int rnd = (int)(Random.value * 4);
        for (int i = 0; i < 4; i++)
        {
            int coner = 2 * (rnd + i) + 1;
            if (IsCellFree(coner))
            {
                return GetCell(coner);
            }
        }
        return MatrixPos.Empty;
    }

    protected virtual MatrixPos GetSideMove()
    {
        int rnd = (int)(Random.value * 4);
        for (int i = 0; i < 4; i++)
        {
            int side = 2 * (rnd + i);
            if (IsCellFree(side))
            {
                return GetCell(side);
            }
        }
        return MatrixPos.Empty;
    }

    protected bool IsCellFree(int i)
    {
        MatrixPos pos = GetCell(i);
        return Field.IsCellFree(pos);
    }
}

public abstract class AIStrategic: AI
{
    protected List<int[]> Strategics = new List<int[]>();
    protected int Mirror;
    protected int Shift;

    public override void Init()
    {
        Mirror = 0;
        Shift = 8;
    }

	protected int GetIndex(MatrixPos pos)
	{
		int i;
		for(i = 0; i < 8; i++)
		    if (pos == MatrixPos.Dir8[i] + new MatrixPos(1, 1))
		    {
		        i -= Shift;
		        i *= Mirror != 0 ? Mirror : 1;
                while (i < 0)
                    i += 8;
                if(i > 8)
		        return i;
		    }

	    return -1;
	}

    protected override MatrixPos GetCell(int i)
    {
        int k = i;
        if (k != -1)
        {
            if (Mirror != 0)
                k *= Mirror;
            k += Shift;
            while (k < 0)
                k += 8;
            k = k%8;
        }
        return base.GetCell(k);
	}

    protected int GetMoveCount()
	{
		int st = 0;
		int i,j;
		for(i = 0; i < Field.Width; i++)
            for (j = 0; j < Field.Width; j++)
				if(!Field.IsCellFree(new MatrixPos(i, j)))
					st++;
		return st;
	}

    protected bool IsCellFree(int i)
	{
		MatrixPos pos = GetCell(i);
		return Field.IsCellFree(pos);
	}

    protected void FileterStrategics()
    {
        int move = GetMoveCount() - 1;
        CheckVariants();
        int last = GetIndex(Field.LastMove);
        for (int i = 0; i < Strategics.Count;)
        {
            if (move < Strategics[i].Length && last == Strategics[i][move])
                i++;
            else
                Strategics.RemoveAt(i);
        }
        DebugStrategics();
    }

    private void CheckVariants()
    {
        int last = GetIndex(Field.LastMove);
        if (last == -1 || (Mirror != 0 && Shift != 8))
            return;
        bool start = Field.GetElement(GetCell(1)) != Element.None;
        if (start && last == 5)
            return;
        int count = 0;
        foreach (var pos in MatrixPos.Dir8)
        {
            if (Field.GetElement(pos + MatrixPos.One) != Element.None)
                count++;
        }
        if (count == 1)
        {
            Shift = last & 6;
        }
        else if (count >= 2)
        {
            if (last > 4)
            {
                Mirror = -1;
                if (start)
                    Shift += 2;
            }
            else
                Mirror = 1;
        }

    }

    protected MatrixPos GetStrategyMove()
    {
        int count = Strategics.Count;
        if (count > 0)
        {
            int rnd = (int) (Random.value*count);
            int move = GetMoveCount();
            if (move < Strategics[rnd].Length)
            {
                int index = Strategics[rnd][move];
                //первый ход в случайный угол
                if (index == 1 && Shift == 8)
                {
                    index += ((int)(Random.value * 4)) * 2;
                }
                return GetCell(index);
            }
        }
        return MatrixPos.Empty;
    }

    public void DebugStrategics()
    {
        String msg = String.Empty;
        foreach (var strategic in Strategics)
        {
            foreach (var i in strategic)
            {
                msg += i + ",";
            }
            msg += "\n";
        }
    }

    public override MatrixPos GetMove()
    {
        int i;
        MatrixPos pos;
        if (Field.LastMove != MatrixPos.Empty)
            FileterStrategics();

        pos = Field.GetWinMove(Sign);
        if (pos != MatrixPos.Empty)
            return pos;

        pos = Field.GetWinMove(Field.GetOpponent(Sign));
        if (pos != MatrixPos.Empty)
            return pos;

        if (Strategics.Count > 0)
        {
            pos = GetStrategyMove();
            if (pos != MatrixPos.Empty)
            {
                return pos;
            }
        }

        if (Field.IsCellFree(MatrixPos.One))
            return MatrixPos.One;

        pos = GetConerMove();
        if (pos != MatrixPos.Empty)
            return pos;

        pos = GetSideMove();
        return pos;
    }

    public override void OnMove()
    {
        FileterStrategics();
    }
}
