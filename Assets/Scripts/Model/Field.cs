﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public enum Element
{
    Tic = 1,
    Tac = -1,
    None = 0
}

public class Field// : MonoBehaviour
{
    protected Element[,] Maze;
    protected Vector2 startPosition;

    public const int Width = 3;
    public static readonly Vector2 FieldCellSize = new Vector2(2f, 2f);

    public delegate void UpdateViewFunc(MatrixPos pos, Element el);
    public static UpdateViewFunc UpdateView;

    public MatrixPos LastMove;

    public virtual MatrixPos ViewStart 
    {
        get {return new MatrixPos(0, 0);}
    }

    public virtual MatrixPos ViewEnd 
    {
        get { return new MatrixPos(Width-1, Width-1);}
    }

    public virtual void Init()
    {
        Maze = new Element[Width, Width];
        //ClearMatrix();
        startPosition = new Vector2(-Width * FieldCellSize.x / 2, -Width * FieldCellSize.y / 2);
    }

    public virtual void Clear()
    {
        ClearMatrix();
        LastMove = MatrixPos.Empty;
        //Maze = null;
    }

    public virtual void ClearMatrix()
    {
        /*if (Maze == null)
            return;*/
        for (int y = 0; y < Width; y++)
        {
            for (int x = 0; x < Width; x++)
            {
                Maze[x, y] = Element.None;
                if(UpdateView != null)
                    UpdateView(new MatrixPos(x, y), Element.None);
            }
        }
    }

    public bool IsPosInRange(MatrixPos pos)
    {
        if (!pos.IsEmpty() && pos.IsPosInRange(new MatrixPos(0, 0), new MatrixPos(Width, Width)))
            return true;
        return false;
    }

    public static Element GetOpponent(Element el)
    {
        return (Element) (-1*(int) el);
    }

    public void SetElement(MatrixPos pos, Element el)
    {
        if (IsPosInRange(pos))
        {
            Maze[pos.x, pos.y] = el;
            if (el != Element.None)
                LastMove = pos;
            if (UpdateView != null)
                UpdateView(pos, el);
        }
        else
        {
            throw new Exception("SetElement: " + pos.ToString());
        }
    }

    public Element GetElement(MatrixPos pos)
    {
        if (IsPosInRange(pos))
        {
            return Maze[pos.x, pos.y];
        }
        throw new Exception("GetElement: " + pos.ToString());
    }


    public MatrixPos CalcGamePosition(Vector2 vec)
    {
        MatrixPos pos = MatrixPos.Empty;

        Vector2 relativePos = vec - startPosition/* - (Vector2)transform.position*/;

        pos.x = (int)(relativePos.x / FieldCellSize.x);
        pos.y = (int)(relativePos.y / FieldCellSize.y);
        return pos;
    }

    public Vector3 CalcPosition(MatrixPos pos, float layer = 0)
    {
        return new Vector3(startPosition.x + (pos.x + 0.5f) * FieldCellSize.x, startPosition.y + (pos.y + 0.5f) * FieldCellSize.y, layer);
    }

    /*public IEnumerable<MatrixPos> GetElementsMatchingCreteria(Predicate<Element> predicate = null)
    {
        return GetElementsMatchingCreteria(predicate, MatrixPos.Range(ViewStart, ViewEnd));
    }

    public IEnumerable<MatrixPos> GetElementsMatchingCreteria(Predicate<Element> predicate, IEnumerable<MatrixPos> selectFrom)
    {
        return from pos in selectFrom
               let el = GetElement(pos)
               where el != null && (predicate == null || predicate(el))
               select pos;
    }*/

    public IEnumerable<MatrixPos> GetEmptyPositions()
    {
        return from pos in MatrixPos.Range(ViewStart, ViewEnd)
            let el = GetElement(pos)
            where el == Element.None
            select pos;
    }

    public MatrixPos GetRandomEmptyPos()
    {
        MatrixPos[] elements = GetEmptyPositions().ToArray();
        return elements[Random.Range(0, elements.Length)];
    }

    public bool IsCellFree(MatrixPos pos)
    {
        return GetElement(pos) == Element.None;
    }

    public int CheckLine(MatrixPos[] line)
    {
        return line.Sum(pos => (int) Maze[pos.x, pos.y]);
    }

    public IEnumerable<MatrixPos> GetCol(int col)
    {
        for (int i = 0; i < Width; i++)
        {
            yield return new MatrixPos(col, i);
        }
    }

    public IEnumerable<MatrixPos> GetRow(int row)
    {
        for (int i = 0; i < Width; i++)
        {
            yield return new MatrixPos(i, row);
        }
    }

    public IEnumerable<MatrixPos> GetDiagonal(int index)
    {
        for (int i = 0; i < Width; i++)
        {
            yield return new MatrixPos(i, index == 0 ? i : 2-i);
        }
    }

    public IEnumerable<MatrixPos[]> GetAllLines()
    {
        for (int i = 0; i < Width; i++)
        {
            yield return GetCol(i).ToArray();
            yield return GetRow(i).ToArray();
            if(i < 2)
                yield return GetDiagonal(i).ToArray();
        }
    }

    public MatrixPos GetWinMove(Element sign)
    {
        foreach (var line in GetAllLines())
        {
            if (CheckLine(line) == 2 * (int)sign)
            {
                foreach (var pos in line)
                {
                    if (IsCellFree(pos))
                        return pos;
                }
            }
        }
        return MatrixPos.Empty;
    }

    public Element CheckWin()
    {
        foreach (var line in GetAllLines())
        {
            int sum = CheckLine(line);
            if (Mathf.Abs(sum) == 3)
            {
                return (Element) Mathf.Sign(sum);
            }
        }
        return Element.None;
    }
}
