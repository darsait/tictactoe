﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public struct MatrixPos : IComparable<MatrixPos>
{
    public int x;
    public int y;


    public MatrixPos(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public MatrixPos Copy()
    {
        return new MatrixPos(x, y);
    }


    public override string ToString()
    {
        if (Equals(Empty))
            return "[EMPTY POS]";
        return "[" + x + ", " + y + "]";
    }


    public int CompareTo(MatrixPos other)
    {
        if (this < other)
            return -1;
        if (this == other)
            return 0;
        return 1;
    }

    public override bool Equals(object o)
    {
        if (o is MatrixPos)
        {
            var other = (MatrixPos) o;
            return other.x == x && other.y == y;
        }
        return false;
    }

    public bool IsEmpty()
    {
        return Equals(Empty);
    }

    public bool IsPosInRange(MatrixPos start, MatrixPos end)
    {
        return x >= start.x && y >= start.y && x < end.x && y < end.y;
    }

    public static bool operator <(MatrixPos pos1, MatrixPos pos2)
    {
        return pos1.y < pos2.y || (pos1.y == pos2.y && pos1.x < pos2.x);
    }

    public static bool operator <=(MatrixPos pos1, MatrixPos pos2)
    {
        return pos1.y < pos2.y || (pos1.y == pos2.y && pos1.x <= pos2.x);
    }

    public static bool operator >(MatrixPos pos1, MatrixPos pos2)
    {
        return pos1.y > pos2.y || (pos1.y == pos2.y && pos1.x > pos2.x);
    }

    public static bool operator >=(MatrixPos pos1, MatrixPos pos2)
    {
        return pos1.y > pos2.y || (pos1.y == pos2.y && pos1.x >= pos2.x);
    }

    public static MatrixPos operator +(MatrixPos pos1, MatrixPos pos2)
    {
        return new MatrixPos(pos1.x + pos2.x, pos1.y + pos2.y);
    }

    public static MatrixPos operator -(MatrixPos pos1, MatrixPos pos2)
    {
        return new MatrixPos(pos1.x - pos2.x, pos1.y - pos2.y);
    }

    public static bool operator ==(MatrixPos pos1, MatrixPos pos2)
    {
        return pos1.x == pos2.x && pos1.y == pos2.y;
    }
    public static bool operator !=(MatrixPos pos1, MatrixPos pos2)
    {
        return pos1.x != pos2.x || pos1.y != pos2.y;
    }

    /*public override int GetHashCode()
    {
        throw new NotImplementedException();
    }*/

    public static readonly MatrixPos Empty = new MatrixPos(Int32.MinValue, Int32.MinValue); 
    public static readonly MatrixPos Zero = new MatrixPos(0, 0);
    public static readonly MatrixPos One = new MatrixPos(1, 1);

    public static readonly MatrixPos DirUp          = new MatrixPos(0, 1);
    public static readonly MatrixPos DirUpRight     = new MatrixPos(1, 1);
    public static readonly MatrixPos DirRight       = new MatrixPos(1, 0);
    public static readonly MatrixPos DirDownRight   = new MatrixPos(1, -1);
    public static readonly MatrixPos DirDown        = new MatrixPos(0, -1);
    public static readonly MatrixPos DirDownLeft    = new MatrixPos(-1, -1);
    public static readonly MatrixPos DirLeft        = new MatrixPos(-1, 0);
    public static readonly MatrixPos DirUpLeft      = new MatrixPos(-1, 1);


    public static readonly MatrixPos[] Dir4 = { DirUp, DirRight, DirDown, DirLeft };
    //public static IEnumerable<MatrixPos> Dir4 { get { return Dir4Array; } }

    public static readonly MatrixPos[] Dir8 = { DirUp, DirUpRight, DirRight, DirDownRight, DirDown, DirDownLeft, DirLeft, DirUpLeft };
    //public static IEnumerable<MatrixPos> Dir8 { get { return Dir8; } }

    public MatrixPos Up() { return new MatrixPos(x, y + 1); }

    public MatrixPos Left() { return new MatrixPos(x - 1, y); }

    public MatrixPos UpLeft() { return new MatrixPos(x - 1, y + 1); }

    public MatrixPos UpRight() { return new MatrixPos(x + 1, y + 1); }

    public MatrixPos Right() { return new MatrixPos(x + 1, y); }

    public MatrixPos Down() { return new MatrixPos(x, y - 1); }

    public MatrixPos DownLeft() { return new MatrixPos(x - 1, y - 1); }

    public MatrixPos DownRight() { return new MatrixPos(x + 1, y - 1); }

    public bool IsNeighbor(MatrixPos pos)
    {
        if (Equals(pos))
            return false;
        return Mathf.Abs(x - pos.x) <= 1 && Mathf.Abs(y - pos.y) <= 1;
    }

    public static IEnumerable<MatrixPos> Range(int bottomLeftX, int bottomLeftY, int upperRightX, int upperRightY)
    {
        for (int y = bottomLeftY; y <= upperRightY; y++)
            for (int x = bottomLeftX; x <= upperRightX; x++)
                yield return new MatrixPos(x, y);
    }

    public static IEnumerable<MatrixPos> Range(MatrixPos start, MatrixPos end)
    {
        var min = new MatrixPos(Mathf.Min(start.x, end.x), Mathf.Min(start.y, end.y));
        var max = new MatrixPos(Mathf.Max(start.x, end.x), Mathf.Max(start.y, end.y));
        return Range(min.x, min.y, max.x, max.y);
    }

 public static IEnumerable<MatrixPos> AroundOfInclusive(MatrixPos pos)
    {
        yield return pos;
        foreach (var dir in Dir8)
            yield return pos + dir;
    }

    public static MatrixPos[] RangeToArray(int bottomLeftX, int bottomLeftY, int upperRightX, int upperRightY)
    {
        return Range(bottomLeftX, bottomLeftY, upperRightX, upperRightY).ToArray();
    }

    public static MatrixPos ArrayMin(IEnumerable<MatrixPos> array)
    {
        var val = Empty;
        foreach (var pos in array)
        {
            if (val.x == Int32.MinValue || val.x > pos.x)
                val.x = pos.x;
            if (val.y == Int32.MinValue || val.y > pos.y)
                val.y = pos.y;
        }
        return val;
    }

    public static MatrixPos ArrayMax(IEnumerable<MatrixPos> array)
    {
        var val = Empty;
        foreach (var pos in array)
        {
            if (val.x == Int32.MinValue || val.x < pos.x)
                val.x = pos.x;
            if (val.y == Int32.MinValue || val.y < pos.y)
                val.y = pos.y;
        }
        return val;
    }

    public static string ArrayToString(IEnumerable<MatrixPos> array)
    {
        string str = String.Empty;
        foreach (var pos in array)
        {
            if (str != String.Empty)
                str += ", ";
            str += pos;
        }
        return str;        
    }

    public static IEnumerable<MatrixPos> NormalizeArray(IEnumerable<MatrixPos> chain)
    {
        var array = chain as MatrixPos[] ?? chain.ToArray();
        var start = ArrayMin(array);
        bool revert = array.First() > array.Last();
        return from pos in revert ? array.Reverse() : array select pos - start;
    }

    public static bool IsCyclic(IEnumerable<MatrixPos> chain)
    {
        var array = chain as IList<MatrixPos> ?? chain.ToArray();
        return array.First().IsNeighbor(array.Last());
    }

    public static bool Compare(IEnumerable<MatrixPos> chain1, IEnumerable<MatrixPos> chain2)
    {
        var array1 = chain1 as MatrixPos[] ?? chain1.ToArray();
        var array2 = chain2 as MatrixPos[] ?? chain2.ToArray();
        if (array1.Length != array2.Length)
            return false;
        for (int i = 0; i < array1.Length; i++)
        {
            if (array1[i] != array2[i])
                return false;
        }
        return true;
    }

    public static int Distance(MatrixPos start, MatrixPos end)
    {
        MatrixPos pos = end - start;
        return Mathf.Abs(pos.x) + Mathf.Abs(pos.y);        
    }

}
